/**
 * @file Main configuration file which imports plugins into the `remark-preset-strictlint` shared NPM configuration package
 * @author Brian Zalewski <help@megabyte.space>
 * @copyright Megabyte LLC 2023
 * @license MIT
 *
 * This file is the entrypoint and shared configuration for our implementation of
 * the unified libraries which include retext (text), remark (Markdown), and possibly markdown-magic.
 * Almost all the official retext plugins are included, remark includes all the plugins
 * related to hosting README.md / other files on GitHub, and rehype includes a healthy
 * dose of plugins for linting HTML as well.
 *
 * Setting environment variables will impact this script:
 *
 * `DISABLE_LINT` - Disables lint features
 * `REMARK_BEHEAD` - Ensures no H1 / # header is exist (it gets turned into a H2 / ##)
 * `REMARK_MAN` - Outputs results in man page format
 * `REMARK_REMOVE` - Removes anything between `<-- REMARK_REMOVE_VALUE:START` and `REMARK_REMOVE_VALUE -->`
 * `REMARK_HTML` - Outputs results in HTML page format
 * `MERMAID_CHROME_EXE` - The path to the Chrome executable you would like to use for the Puppeteer instance necessary to render markdown charts with https://mermaid.js.org/
 *
 * @summary This file is a shared configuration and can be controlled by environment variables
 *
 * @todo Integrate markdown-magic (config in the root of this repository) so that it runs before Remark in this configuration. It would be easy to do if we could figure out how to get the name of the current file getting processed by Remark and then run markdown-magic CLI on the file before processing it.
 *
 * @see {@link https://megabyte.space/docs/linting|Linting}
 * @see {@link https://megabyte.space/docs/cli/variables|Environment Variables}
 */

import { codeImport as remarkCodeImport } from 'remark-code-import'
import { readFileSync } from 'node:fs'
import { unified } from 'unified'
import * as remarkOembed from 'remark-oembed'
import dictionary from 'dictionary-en-gb'
import remarkBehead from 'remark-behead'
import remarkCapitalize from 'remark-capitalize'
import remarkComments from 'remark-comments'
import remarkContributors from 'remark-contributors'
// Waiting on a response from this GitHub issue: https://github.com/sergioramos/remark-copy-linked-files/issues/366
// import remarkCopyLinkedFiles from 'remark-copy-linked-files'
import remarkFrontmatter from 'remark-frontmatter'
import remarkGfm from 'remark-gfm'
import remarkGitContributors from 'remark-git-contributors'
import remarkGithub from 'remark-github'
import remarkGithubBetaBlockquoteAdmonitions from 'remark-github-beta-blockquote-admonitions'
import remarkHtml from 'remark-html'
import remarkIgnore from 'remark-ignore'
import remarkLicense from 'remark-license'
import remarkMan from 'remark-man'
import remarkMath from 'remark-math'
import remarkMermaid from 'remark-mermaidjs'
import remarkNormalizeHeadings from 'remark-normalize-headings'
import remarkParse from 'remark-parse'
import remarkPresetLintConsistent from 'remark-preset-lint-consistent'
import remarkPresetLintMarkdownStyleGuide from 'remark-preset-lint-markdown-style-guide'
import remarkPresetLintRecommended from 'remark-preset-lint-recommended'
import remarkPrettier from 'remark-prettier'
import remarkPrism from 'remark-prism'
import remarkRemoveComments from 'remark-remove-comments'
import remarkRemoveTrailingSlash from 'remark-remove-url-trailing-slash'
import remarkRetext from 'remark-retext'
import remarkStripHtml from 'remark-strip-html'
import remarkToc from 'remark-toc'
import remarkValidateLinks from 'remark-validate-links'
import remarkYamlConfig from 'remark-yaml-config'
import retextAssuming from 'retext-assuming'
import retextCasePolice from 'retext-case-police'
import retextContractions from 'retext-contractions'
import retextEmoji from 'retext-emoji'
import retextEnglish from 'retext-english'
import retextEquality from 'retext-equality'
import retextIndefiniteArticle from 'retext-indefinite-article'
import retextIntensify from 'retext-intensify'
import retextKeywords from 'retext-keywords'
import retextLatin from 'retext-latin'
import retextOveruse from 'retext-overuse'
import retextPassive from 'retext-passive'
import retextProfanities from 'retext-profanities'
import retextQuotes from 'retext-quotes'
import retextReadability from 'retext-readability'
import retextRedundantAcronyms from 'retext-redundant-acronyms'
import retextRepeatedWords from 'retext-repeated-words'
import retextSentenceSpacing from 'retext-sentence-spacing'
import retextSimplify from 'retext-simplify'
import retextSpell from 'retext-spell'
import retextSyntaxMentions from 'retext-syntax-mentions'
import retextSyntaxUrls from 'retext-syntax-urls'
import retextUsage from 'retext-usage'

/**
 * Read `package.json` to determine which features we can enable (since they
 * depend on specific data keys in the file). The `remark-license` plugin, for
 * instance, relies on the license key in `package.json` being populated.
 */
const pkgData = readFileSync('package.json')
const pkg = JSON.parse(pkgData.toString())

/**
 * Used by [remarkPlugins] to include all the retext plugins when the `DISABLE_LINT`
 * environment variable is not set.
 *
 * @returns unified retext plugin set
 */
const retextPlugins = () =>
  unified()
    .use(retextEnglish)
    .use(retextLatin)
    .use(retextAssuming)
    .use(retextContractions)
    .use(retextEmoji)
    .use(retextEquality)
    .use(retextIndefiniteArticle)
    .use(retextIntensify)
    .use(retextKeywords)
    .use(retextOveruse)
    .use(retextPassive)
    .use(retextProfanities)
    .use(retextQuotes)
    .use(retextReadability)
    .use(retextRedundantAcronyms)
    .use(retextRepeatedWords)
    .use(retextSentenceSpacing)
    .use(retextSimplify)
    .use(retextSpell, dictionary)
    .use(retextSyntaxMentions)
    .use(retextSyntaxUrls)
    .use(retextUsage)
    .use(retextCasePolice)

/**
 * Defines the master list of remark plugins to include.
 */
const remarkPlugins = [
  remarkParse,
  remarkGfm,
  [remarkFrontmatter, ['yaml', 'toml']],
  remarkYamlConfig,
  remarkToc,
  remarkOembed,
  remarkMath,
  remarkCapitalize,
  remarkCodeImport,
  pkg.repository && remarkGithub,
  remarkGithubBetaBlockquoteAdmonitions,
  remarkIgnore,
  pkg.license && remarkLicense,
  remarkNormalizeHeadings,
  [
    remarkPrism,
    {
      plugins: [
        'autolinker',
        'command-line',
        'data-uri-highlight',
        'diff-highlight',
        'inline-color',
        'keep-markup',
        'line-numbers',
        'treeview'
      ]
    }
  ],
  process.env.REMARK_REMOVE && [remarkComments, { beginMarker: process.env.REMARK_REMOVE + ':START', endMarker: process.env.REMARK_REMOVE }],
  remarkRemoveComments,
  remarkRemoveTrailingSlash,
  process.env.MERMAID_CHROME_EXE && [remarkMermaid, {
    executablePath: process.env.MERMAID_CHROME_EXE,
  }],
  process.env.REMARK_BEHEAD && [remarkBehead, { minDepth: 2 }],
  remarkPrettier,
  pkg.contributors && (remarkContributors || remarkGitContributors),
  !process.env.DISABLE_LINT && remarkPresetLintConsistent,
  !process.env.DISABLE_LINT && remarkPresetLintMarkdownStyleGuide,
  !process.env.DISABLE_LINT && remarkPresetLintRecommended,
  !process.env.DISABLE_LINT && remarkValidateLinks,
  !process.env.DISABLE_LINT && [remarkRetext, retextPlugins()],
  process.env.REMARK_MAN && remarkStripHtml,
  process.env.REMARK_MAN && remarkMan,
  // process.env.REMARK_HTML && remarkCopyLinkedFiles,
  process.env.REMARK_HTML && remarkHtml
]

/**
 * Structures the plugin list / configurations in a way that remark's CLI can digest when
 * the NPM package name is specified in the cosmicConfig reference in package.json.
 */
const documentLintConfig = {
  plugins: remarkPlugins.filter(Boolean)
}

export default documentLintConfig
