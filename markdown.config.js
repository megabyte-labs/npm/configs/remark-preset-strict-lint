module.exports = {
    matchWord: 'AUTO-GENERATED',
    transforms: {
      /* Match <!-- AUTO-GENERATED:START (DEPENDENCIES) --> */
      DEPENDENCIES: require('markdown-magic-dependency-table'),
      /* <!-- AUTO-GENERATED:END --> */
      DIRTREE: require('markdown-magic-directory-tree'),
      ENGINES: require('markdown-magic-engines'),
      INSTALLCMD: require('markdown-magic-install-command'),
      SCRIPTS: require('markdown-magic-package-scripts'),
      SUBPACKAGES: require('markdown-magic-subpackage-list')
    },
    callback: function () {
      console.log('markdown-magic processing done!')
    }
  }
};